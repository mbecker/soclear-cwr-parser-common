package org.waxeye.test;

import org.waxeye.parser.ParseResult;

public class Application {
    public static void main(String[] args) {
        final Parser parser = new Parser();
        final ParseResult<Type> result = parser.parse("NWR0000000000000000DEFENCE                                                       XXXXXXXXXXXXXX           19700101            UNC000405U      ORI         XXXXXX XXXXX                  DE01000051  N000                                                            \n");


        System.out.println(result);
    }
}
