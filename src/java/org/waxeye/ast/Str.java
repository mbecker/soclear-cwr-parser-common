package org.waxeye.ast;

public class Str<E extends Enum<?>> extends NoChildren<E> implements IString {
    private final String value;
    private final int position;

    public Str(E type, String value, int position) {
        super(type);
        this.value = value;
        this.position = position;
    }

    @Override
    public void acceptASTVisitor(IASTVisitor visitor) {
        visitor.visitString(this);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getPos() {
        return position;
    }

    @Override
    public int length() {
        return value.length();
    }

    @Override
    public String toString() {
        return "Str{" +
            "value='" + value + '\'' +
            ", position=" + position +
            '}';
    }
}
