package org.waxeye.ast;

public interface IString {
    String getValue();

    int getPos();

    int length();
}
