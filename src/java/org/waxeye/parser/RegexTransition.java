package org.waxeye.parser;

import org.waxeye.ast.IAST;

public class RegexTransition<E extends Enum<?>> implements ITransition<E> {
    /**
     * Frequency of characters
     */
    private final int frequency;
    private final CharTransition<E> charTransition;


    public RegexTransition(int frequency, char[] single, char[] min, char[] max) {
        this(frequency, new CharTransition<>(single, min, max));
    }

    public RegexTransition(int frequency, CharTransition<E> charTransition) {
        this.frequency = frequency;
        this.charTransition = charTransition;
    }

    public int getFrequency() {
        return frequency;
    }

    public boolean matches(String string) {
        //System.out.println("matching " + string);
        //System.out.println("\tlength: " + (string.length() == frequency));

        if (string.length() != frequency) {
            return false;
        }

        for (char c : string.toCharArray()) {
            //System.out.println("\twithin set(" + c + "): " + charTransition.withinSet(c));

            if (!charTransition.withinSet(c)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public IAST<E> acceptVisitor(ITransitionVisitor<E> visitor) {
        return visitor.visitRegexTransition(this);
    }
}
