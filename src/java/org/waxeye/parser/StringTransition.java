package org.waxeye.parser;

import org.waxeye.ast.IAST;

public class StringTransition<E extends Enum<?>> implements ITransition<E> {
    private final String string;

    public StringTransition(String string) {
        this.string = string;
    }

    @Override
    public IAST<E> acceptVisitor(ITransitionVisitor<E> visitor) {
        return visitor.visitStringTransition(this);
    }

    public int getStringLength() {
        return string.length();
    }

    public boolean matches(String string) {
        return string != null && string.equals(this.string);
    }

    @Override
    public String toString() {
        return "StringTransition{" +
            "string='" + string + '\'' +
            '}';
    }
}
