<?php


namespace parser\expression;


class StringExpression extends Expression
{
    private string $value;

    /**
     * StringExpression constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        parent::__construct(ExpressionType::STRING);

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
