<?php


namespace parser\ast;

/**
 * An AST representing a nonterminal with only chars as children.
 *
 * For convenience, this AST can be displayed as {"type": "$name", "value": "$charsOfChildren"}
 * @package parser\ast
 */
class NTChar extends AST
{
    private string $name;
    private string $value;
    private int $start;
    private int $end;

    /**
     * NTChar constructor.
     * @param string $name
     * @param string $value
     * @param int $position
     */
    public function __construct(string $name, string $value, int $position)
    {
        parent::__construct("NTChar", new ASTs(), $position, $position + strlen($value));

        $this->name = $name;
        $this->value = $value;
        $this->start = $position;
        $this->end = $position + strlen($value);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
