<?php

use parser\config\Automata;
use parser\config\Automaton;
use parser\config\ParserConfig;
use parser\expression\Expression;
use parser\expression\Expressions;
use parser\NonTerminalMode;
use parser\Parser;

class GenParser extends Parser
{
    public function __construct()
{
        $automata = new Automata();

$automata["senderId"] = new Automaton("senderId", NonTerminalMode::NORMAL, Expression::AltExpression(Expression::CharClassLgExpression(9, Expression::CharClassExpression(array(), array(48), array(57))), Expression::CharClassLgExpression(11, Expression::CharClassExpression(array(), array(48), array(57)))));
$config = new ParserConfig($automata, "senderId");
parent::__construct($config);
}
}
